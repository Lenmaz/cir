"""
MAI 2018-2019

Cognitive Interaction with Robots

Adapted from https://www.pyimagesearch.com/2018/06/18/face-recognition-with-opencv-python-and-deep-learning/
by Adrian Rosebrok

Adaptation by Antonio Romero, Manel Rodriguez, Bartomeu Pou
"""

from imutils.video import VideoStream
import face_recognition
import imutils
import pickle
import time
import cv2
import qlearner
import os
import shutil
from encode_faces import obtain_encodings

"""
recognize_faces_video.py is the face recognition part of our recommender

"""



def video_input(env, engine, eps, void_emb):
    """
    This function is the face recognition part of our recommender
    """

    # model to use 'hog' or 'cnn'
    model = 'hog'
    # if we display the image detected
    display = 0

    # load the known faces and embeddings

    if not void_emb:
        data = pickle.loads(open("pictures/encodings.pickle", "rb").read())
    else:
        if os.path.exists("information"):
            pass
            #shutil.rmtree("information")
        for _, dirs, _ in os.walk("pictures/"):
            for dir in dirs:
                if dir != 'void_encodings':
                    shutil.rmtree("pictures/" + dir)
        data = pickle.loads(open("pictures/void_encodings/encodings.pickle", "rb").read())

    vs = VideoStream(src=0).start()
    time.sleep(1.0)

    # How many pictures an unknown person has taken for our agent to remember
    total_pictures = 0
    # Counter known and unknown counts how many frames we have detected a known/unknown person
    counter_known = 0
    counter_unknown = 0
    # boolean needed for the first time we detect an unknown person
    bool_unknown_text = False
    # detecting boolean to know if we are still in the detecting part
    detecting = True

    # Names the face recognition detects
    names_write = []

    while detecting:
        names = []
        frame = vs.read()

        # convert the input frame from BGR to RGB then resize it to have
        # a width of 750px (to speedup processing)
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        rgb = imutils.resize(rgb, width=750)
        r = frame.shape[1] / float(rgb.shape[1])

        # detect the (x, y)-coordinates of the bounding boxes
        # corresponding to each face in the input frame, then compute
        # the facial embeddings for each face
        boxes = face_recognition.face_locations(rgb,model=model)
        encodings = face_recognition.face_encodings(rgb, boxes)

        # loop over the facial embeddings
        for encoding in encodings:
            # attempt to match each face in the input image to our known
            # encodings
            matches = face_recognition.compare_faces(data["encodings"],encoding)
            name = "Unknown"

            # check to see if we have found a match
            if True in matches:
                # find the indexes of all matched faces then initialize a
                # dictionary to count the total number of times each face
                # was matched
                matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                counts = {}

                # loop over the matched indexes and maintain a count for
                # each recognized face face
                for i in matchedIdxs:
                    name = data["names"][i]
                    counts[name] = counts.get(name, 0) + 1

                # determine the recognized face with the largest number
                # of votes (note: in the event of an unlikely tie Python
                # will select first entry in the dictionary)
                name = max(counts, key=counts.get)

            # update the list of names
            print("Person detected: ", name)
            names.append(name)

            # We check if we know the person detected or not and update a counter
            if detecting and name != 'Unknown':
                names_write.append(name)
                names_write = list(set(names))
                counter_known = counter_known + 1
            elif detecting and name == 'Unknown':
                counter_unknown += 1

        # If we have detected a known person more than 20 frames we set detecting False and we will start the qlearning
        if counter_known > 20 and detecting:
                detecting = False

        # If we have detected a unknown person more than 20 frames we will ask him to take pictures
        # to create new embedings and ask for the name

        if 'Unknown' in names and len(set(names))==1 and counter_unknown > 20:

            if not bool_unknown_text:

                display = 1
                qlearner.engine_say(env, engine, "Detecting unknown person")
                engine.runAndWait()

                temp_name = "temporal_name"
                # Implementar que new name no este en el set

                qlearner.engine_say(env, engine, "Hello new unknown person, as you are a new user we need to do pictures of yourself")
                cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
                cv2.setWindowProperty("window", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
                qlearner.engine_say(env, engine,"Press k to do pictures of yourself")

                bool_unknown_text = True

            if key == ord("k") and total_pictures < 5:

                orig = frame.copy()
                if not os.path.exists('pictures/'+temp_name):
                    os.makedirs('pictures/'+temp_name)
                p = os.path.sep.join(['pictures/' + temp_name, "{}.png".format(
                    str(total_pictures).zfill(5))])
                cv2.imwrite(p, orig)
                total_pictures += 1
                qlearner.engine_say(env, engine, str(total_pictures) + " out of 5")
                if total_pictures > 4:
                    cv2.destroyAllWindows()
                    display = 0
                    qlearner.engine_say(env, engine, "Very good, Finally I need your name to know you better")
                    env.window.need_input = True
                    new_name = None
                    while new_name is None:
                        new_name = env.window.input
                    os.rename('pictures/'+temp_name, 'pictures/'+new_name)
                    os.makedirs('information/' + new_name)
                    qlearner.engine_say(env, engine,"Now give me a second to study your face so I will remember you")
                    obtain_encodings()
                    detecting = False
                    env.window.input = None
                    names_write = [new_name]
                    qlearner.engine_say(env, engine, "Perfect, give me a moment to initialise and I will start recommending songs")


        # loop over the recognized faces and paint bounding boxes

        for ((top, right, bottom, left), name) in zip(boxes, names):
            # rescale the face coordinates
            top = int(top * r)
            right = int(right * r)
            bottom = int(bottom * r)
            left = int(left * r)

            # draw the predicted face name on the image
            cv2.rectangle(frame, (left, top), (right, bottom),
                (0, 255, 0), 2)
            y = top - 15 if top - 15 > 15 else top + 15
            cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
                0.75, (0, 255, 0), 2)

        # Display what is detected, this will only work when a unknown person is detected for more than 20 frames
        if display > 0:
            cv2.imshow("window", frame)
        key = cv2.waitKey(1) & 0xFF

    # after setting detecting false we will delete the videostreamer object and start the recommender part
    if not detecting:
        vs.stop()
        del vs
        qlearner.q_learning(env, engine, eps, names_write)