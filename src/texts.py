"""
MAI 2018-2019

Cognitive Interaction with Robots

@authors Antonio Romero, Manel Rodriguez, Bartomeu Pou
"""

from window import Window
import time
from datetime import datetime
import numpy as np


"""
texts.py consists of a set of functions that change the recommenders text/voice based on some variables:

- Last rewards
- Time of the day
- How long since it saw the user for the last time

Additionally it may say different things randomly

"""

def get_date():
    date = time.strftime("%Y-%m-%d")
    return date


def days_between(d1, d2):
    d1 = datetime.strptime(d1, "%Y-%m-%d")
    d2 = datetime.strptime(d2, "%Y-%m-%d")
    return abs((d2 - d1).days)


class TextHandler:
    NB_VISITS = 100
    LAST_VISIT = "2014-11-23"
    LAST_FEELING = Window.STATE_SAD

    def __init__(self, user):

        self.user = user
        print("User:", user)
        try:
            self.load()
        except:
            TextHandler.NB_VISITS = 0
            TextHandler.LAST_VISIT = "2014-11-23"
            TextHandler.LAST_FEELING = Window.STATE_NORMAL

    def load(self):
        print('Then: ', TextHandler.NB_VISITS, TextHandler.LAST_VISIT, TextHandler.LAST_FEELING)
        info = np.load("information/" + self.user + "/info.npy")
        TextHandler.NB_VISITS = info[0]
        TextHandler.LAST_VISIT = str(info[1]) + "-" + str(info[2]) + "-" + str(info[3])
        TextHandler.LAST_FEELING = info[4]

        print('NOW', TextHandler.NB_VISITS, TextHandler.LAST_VISIT, TextHandler.LAST_FEELING)

    def save(self):

        info = list()

        info.append(TextHandler.NB_VISITS + 1)
        date = time.strftime("%Y-%m-%d").split("-")

        for d in date:
            info.append(int(d))
        info.append(TextHandler.LAST_FEELING)
        np.save("information/" + self.user + "/info.npy", info)

    def present_song(self, song, state = Window.STATE_NORMAL, last_reward=0):

        TextHandler.LAST_FEELING = state
        possible_texts = list()
        possible_texts.append('Let me show you: ' + song + '.')
        possible_texts.append('I think you may like: ' + song + '.')
        possible_texts.append('You might enjoy: ' + song + '.')
        possible_texts.append('Now I got this song for you: ' + song + '.')
        possible_texts.append('Alright then, then lets move to: ' + song)

        if TextHandler.NB_VISITS == 0:
            possible_texts.append('Now listen to the song: ' + song + '.')
            possible_texts.append('I am not sure but lets go with: ' + song + '.')

        if last_reward < -2:
            possible_texts.append('Sorry about the last one. Let me try with: ' + song)
            possible_texts.append('Sorry about the last one. Now listen to: ' + song)
            possible_texts.append('Perhaps you will prefer: ' + song + '.')

            if last_reward < -4:
                possible_texts.append('Sorry! Sorry! Lets continue with: ' + song)
        elif last_reward > 2:
            possible_texts.append('Yay, I think I got it. Now how about: ' + song + '?')
            possible_texts.append('Good, good. Lets move to: ' + song)
            possible_texts.append('I hope you will enjoy this one as much: ' + song + '.')

        if TextHandler.LAST_FEELING == Window.STATE_SAD:
            possible_texts.append('I think this will cheer you up: ' + song)
            possible_texts.append('When Im sad I like to listen to: ' + song)
        elif TextHandler.LAST_FEELING == Window.STATE_HAPPY:
            if TextHandler.NB_VISITS > 1:
                possible_texts.append('Let me reward your smile with: ' + song)
            elif TextHandler.NB_VISITS > 3:
                possible_texts.append('I have this feel good song for you: ' + song)
            possible_texts.append('Now a positive song: ' + song)
            possible_texts.append('Lets continue with this cool song: ' + song)

        where = np.random.randint(len(possible_texts))
        text = possible_texts[where]

        return text

    def greeting(self):

        days_since_last_visit = days_between(TextHandler.LAST_VISIT, get_date())
        possible_texts = list()

        if TextHandler.NB_VISITS == 0:
            possible_texts.append('Hello ' + self.user + ', your wish is my command.')
            possible_texts.append('Welcome ' + self.user + ', your wish is my command.')
        else:
            if days_since_last_visit < 1:
                possible_texts.append('Hi, ' + self.user + '. Glad to see you back so soon.')
            else:
                possible_texts.append('Long time no see, ' + self.user + '!')

            possible_texts.append('Welcome back, ' + self.user + '.')

            hour = int(time.strftime("%H"))

            if hour < 6 or hour > 21:
                possible_texts.append('Good night, ' + self.user + '. Welcome back.')
            elif hour < 12:
                possible_texts.append('Good morning, ' + self.user + '. Your presence is always appreciated.')
            elif hour < 19:
                possible_texts.append('Good afternoon, ' + self.user + '. I have some cool music prepared for you.')
            else:
                possible_texts.append('Good evening, ' + self.user + '. Thanks for choosing me again.')

        where = np.random.randint(len(possible_texts))
        text = possible_texts[where]

        return text

    def say_goodbye(self):
        hour = int(time.strftime("%H"))

        possible_texts = list()
        possible_texts.append('Good bye, ' + self.user + '. Hope you had fun!')
        possible_texts.append('Well, ' + self.user + ', see you soon!')

        if hour < 6 or hour > 21:
            if TextHandler.NB_VISITS > 2:
                possible_texts.append('Bye, ' + self.user + '. Sweet dreams  tonight!')
            else:
                possible_texts.append(self.user + ', sleep well. Cheers.')
        elif hour < 12:
            possible_texts.append('Bye, bye, ' + self.user + '. Have a nice day.')
            possible_texts.append('Bye, bye, ' + self.user + '. Enjoy your day!')
        else:
            possible_texts.append('See ya, ' + self.user + '. Hope you enjoy the rest of the day.')

        where = np.random.randint(len(possible_texts))
        text = possible_texts[where]

        return text

    def ask_feelings(self):
        days_since_last_visit = days_between(TextHandler.LAST_VISIT, get_date())
        possible_texts = list()
        hour = int(time.strftime("%H"))

        if TextHandler.NB_VISITS == 0:
            possible_texts.append('Tell me how you feel right now, please.')
            possible_texts.append('Would you kindly tell me how you feel?')
        else:
            if TextHandler.LAST_FEELING == Window.STATE_HAPPY:
                possible_texts.append('Last time your good mood made me happy too. Are you still happy?')
            if TextHandler.LAST_FEELING == Window.STATE_SAD:
                possible_texts.append('Have you cheered up since last time? I hope so!')
                possible_texts.append('Last time I saw you a bit down.  How are you now?')

            if days_since_last_visit < 1:
                possible_texts.append('How are you now?')
                possible_texts.append('How do you feel now?')
            else:
                if 21 > hour > 6:
                    possible_texts.append('How are you today?')
                    possible_texts.append('How do you feel today?')
                else:
                    possible_texts.append('How do you feel tonight?')
                    possible_texts.append('How are you tonight?')

        where = np.random.randint(len(possible_texts))
        text = possible_texts[where]
        return text
