"""
MAI 2018-2019

Cognitive Interaction with Robots

@authors Antonio Romero, Manel Rodriguez, Bartomeu Pou
"""

import numpy as np
from collections import deque
import pyttsx3
import time
import threading
import pandas as pd
from window import Window
import recognize_faces_video
from texts import TextHandler

"""
q_learner.py is the recommender main functionality which recommends songs

"""

NB_SINGERS = 10
NB_GENRES = 10
NB_DECADES = 1

def engine_say(env, engine, text):
    """
    Function that makes our recommender say in text and voice the input text
    """
    engine.say(text)
    env.window.output = text
    engine.runAndWait()


def voice_engine():
    """
    Function to initialise the voice for our recommender
    """
    engine = pyttsx3.init()
    engine.setProperty('rate', 135)
    return engine


class Heuristic:
    """
    Predictor class to update weights and control features
    """

    def __init__(self, nb_singers, nb_genres, means, n, weights):

        self.means = means
        self.n = n
        self.weights = weights

        for _ in range(3):
            self.means.append([np.zeros(nb_singers), np.zeros(nb_genres)])
            self.n.append([np.zeros(nb_singers), np.zeros(nb_genres)])

    def re_calculate(self, song, state, q_value):

        features = [song.artist_id, song.genre_id]

        for i in range(len(features)):
            self.n[state][i][features[i]] += 1
            # numero de states [happy,sad,normal] x [numero de features (genero,cantante)] x numero de elementos de cada features, por ejemplo cantantes 10
            m_n = self.means[state][i][features[i]]

            self.means[state][i][features[i]] = m_n + (q_value - m_n) / float(self.n[state][i][features[i]])


    def predict(self, song, state):

        features = [song.artist_id, song.genre_id]

        prediction = [0 for _ in features]

        for i in range(len(features)):
            k = self.means[state][i][features[i]]
            if k == 0:
                mean_improvised = 0
                n_impro = 0
                for s in range(3):
                    w = self.means[s][i][features[i]]
                    if w != 0:
                        n_impro += 1
                        mean_improvised += w
                if n_impro > 0:
                    mean_improvised /= float(n_impro)
                    k = mean_improvised
            prediction[i] = k
        new_q = self.weights[0]*prediction[0] + self.weights[1]*prediction[1]

        return new_q


def heuristic_approximator(h, song, qtable, state_size, songs, visited_state_actions):
    """
    Linear approximator for the Q-learning.
    Values near the updated value are updated too even if we have never seen them.

    """

    for index, row in songs.iterrows():
        if row.artist_id == song.artist_id or row.genre_id == song.genre_id:
            for state in range(state_size):
                if visited_state_actions[state, row.iloc[0]] == 0:
                    qtable[state, row.iloc[0]] = h.predict(row, state)


class Environment:
    """
    Environment for our recommender
    Usual functions as an openAI environment:
    Render: to render the face
    Step: s,a -> s',r,done
    """

    def __init__(self, df):

        self.done = False
        self.df = df
        self.listened_songs = deque([], 10)
        self.window = Window(self)
        self.reward = None
        self.textHandler = None

    def setTextHandler(self,textHandler):
        self.textHandler = textHandler

    def render(self):
        self.window.create()

    def step(self, action, state, last_reward, engine, environment):

        listened_song = self.df.loc[action, 'index']
        self.listened_songs.append(listened_song)


        song = str(self.df.loc[action, 'track_id'])
        song_title = str(self.df.loc[action, 'track_title'])


        machine_text = environment.textHandler.present_song(song_title, state, last_reward)
        engine_say(environment, engine, machine_text)

        while len(song) < 6:
            song = '0' + song
        get = song[0:3] + '/' + song + '.mp3'
        self.window.mixer.music.load('songs/fma_supersmall/' + get)
        t = time.time()
        self.window.mixer.music.play()

        while self.reward is None:
            time_taken = time.time() - t

            if time_taken > 30:
                self.window.input = None
                self.window.mixer.music.stop()
                reward = 5
                self.reward = reward
            else:
                try:
                    reward = float(self.window.input)
                    self.window.input = None
                    self.window.mixer.music.stop()

                    # Change the scale of the reward seconds to range (-5,5)
                    reward = time_taken/float(3) - 5
                    reward = np.clip(reward, -5, 5)

                    self.reward = reward
                except:
                    pass
        self.reward = None

        return self.listened_songs, reward, self.done


def choose_action(qtable, eps, state, previous_actions):
    """
    Choose action epsilon greedy
    """

    qtable2 = qtable[state].copy()

    for act in previous_actions:
        qtable2[act] = -10000000

    if np.random.random() <= eps:
        print(eps)
        random_act = np.random.randint(100)
        if len(previous_actions) > 0:
            random_act = previous_actions[0]

        while random_act in previous_actions:
            random_act = np.random.randint(100)
        return random_act

    else:
        options_pool = qtable2.argsort()[-5:][::-1]

        chosen = np.random.randint(5)
        resultado = options_pool[chosen]

        return resultado


def save_q_table(q_table, predict, visited_state_actions, user):
    folder = 'information/' + user
    np.save(folder + "/qtable.npy", q_table)
    np.save(folder + "/means.npy", np.array(predict.means))
    np.save(folder + "/n.npy", np.array(predict.n))
    np.save(folder + "/weights.npy", np.array(predict.weights))
    np.save(folder + "/visited_state_actions.npy", visited_state_actions)
    print('Successfully saved results.')


def load_q_table(user, state_size, action_size):
    """
    Loads q-tables of the users that used the recommender before
    """

    def get_q_table(user):
        folder = 'information/' + user
        q_table = np.load(folder + "/qtable.npy")
        means = np.load(folder + "/means.npy")
        n = np.load(folder + "/n.npy")
        weights = np.load(folder + "/weights.npy")
        visited_state_actions = np.load(folder + "/visited_state_actions.npy")

        return q_table, means, n, weights, visited_state_actions

    try:
        q_table, means, n, weights, visited_state_actions = get_q_table(user)
    except:
        q_table = np.zeros((state_size, action_size))
        means = list()
        n = list()
        weights = [0.5, 0.5]
        visited_state_actions = np.zeros((state_size, action_size))

    return q_table, list(means), list(n), list(weights), visited_state_actions



def select_boss(engine, env, users):
    """
    Function to select the leader user
    """
    engine_say(env, engine, "I am detecting more than one user, who will be the leader?")
    ok = False
    while not ok:
        y = None
        while y is None:
            y = env.window.input
        for i in range(len(users)):
            if y[0:3] == users[i][0:3].lower():
                user_boss = users[i]
                ok = True
                env.window.input = None

        if not ok:
                engine_say(env, engine, "Invalid user, please write again")

    return user_boss


def q_learning(env, engine, epsilon, users):
    """
    Q-learning function for the recommender system. It is called from the recognize_faces_video.py
    """

    stats = list()
    print("epsilon is ", epsilon)
    if len(users) > 1:
        user = select_boss(engine, env, users)
    else:
        user = users[0]

    env.setTextHandler(TextHandler(user))

    env.window.recognised = True
    env.window.first_time = env.textHandler.NB_VISITS == 0
    text1 = env.textHandler.greeting()
    engine_say(env, engine, text1)

    text2 = env.textHandler.ask_feelings()
    engine_say(env, engine, text2)

    env.window.allow_start = True

    state_size = 3
    action_size = len(env.df)

    qtable, means, n, weights, visited_state_actions = load_q_table(user, state_size, action_size)



    heur = Heuristic(NB_SINGERS, NB_GENRES, means, n, weights)

    # Hyperparameters
    previous_actions = deque([], 7)
    done = False
    reward = 0
    while env.window.pause:
        pass
    for _ in range(10):
    #while not done:
         if not done:
            if not env.window.pause:
                state = env.window.state

                action = choose_action(qtable, epsilon, state, previous_actions)
                previous_actions.append(action)

                visited_state_actions[state, action] += 1
                _, reward, done = env.step(action,  state, reward, engine, env)

                stats.append([state, reward])
                if not done:
                    # Qtable update
                    if visited_state_actions[state, action] == 1:
                        qtable[state, action] = reward
                    else:
                        qtable[state, action] += (reward - qtable[state, action])/float(visited_state_actions[state, action])

                    heur.re_calculate(env.df.loc[action], state, qtable[state, action])

                    heuristic_approximator(heur, env.df.loc[action], qtable, state_size, env.df, visited_state_actions)
    env.window.sleeping = True
    engine_say(env, engine, env.textHandler.say_goodbye())
    env.textHandler.save()
    np.save("information/" + user + "/stats.npy", stats)
    if epsilon < 1.0:
        save_q_table(qtable, heur, visited_state_actions, user)
    env.window.can_finish = True
    print("-------Finished--------")
    time.sleep(2)


class QLearner:
    """
    A Wrapper for the Q-learning method, which uses multithreading
    in order to handle the game rendering.
    """
    def __init__(self, eps, void_emb = False, drawing=True):
        dataset = pd.read_csv('songs/resultFmaSmall100.csv')

        eng = voice_engine()
        voices = eng.getProperty('voices')
        eng.setProperty('voice', voices[1].id)
        env = Environment(df=dataset)

        engine_say(env, eng, "Initialising Systems, Please wait")

        thread1 = threading.Thread(target=recognize_faces_video.video_input, args=(env, eng, eps, void_emb,))

        thread1.daemon = True
        thread1.start()
        if drawing:
            env.render()