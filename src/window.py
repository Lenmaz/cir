"""
MAI 2018-2019

Cognitive Interaction with Robots

@authors Antonio Romero, Manel Rodriguez, Bartomeu Pou
"""

import pygame
import time
import sys
import os

"""
Window.py is the interface of the recommender

It consists of the face and the interaction user/agent

"""


class Window:

    STATE_HAPPY = 0
    STATE_NORMAL = 1
    STATE_SAD = 2

    HAPPY_COLOR = (128, 100, 0)
    NEUTRAL_COLOR = (0, 100, 0)
    SAD_COLOR = (100, 0, 100)

    def __init__(self,env):
        """
        Initializes all the important variables needed.

        :param info: all the necessary information extracted from an Environment Class Instance.
        """

        directory = "graphics/"
        self.background = pygame.image.load(directory + "background.png")
        self.happy_face = pygame.image.load(directory + "happy.png")
        self.sad_face = pygame.image.load(directory + "sad.png")
        self.normal_face = pygame.image.load(directory + "normal.png")
        self.initial_face = pygame.image.load(directory + "initialising.png")
        self.nervous_face = pygame.image.load(directory + "nervous.png")

        self.sleep_face = pygame.image.load(directory + "sleep.png")
        self.recognise_face = pygame.image.load(directory + "heart.png")

        self.text_background = pygame.image.load(directory + "fortext.png")

        self.sleep_face = pygame.transform.scale(self.sleep_face, (480, 480))
        self.recognise_face = pygame.transform.scale(self.recognise_face, (480, 480))
        self.text_background = pygame.transform.scale(self.text_background, (400, 60))
        self.text_background_large = pygame.transform.scale(self.text_background, (1000, 60))



        self.sleeping = False
        self.env = env
        self.width = 1080
        self.height = 720

        self.state = 1
        self.input = None

        self.output = 'Output'

        self.mixer = pygame.mixer

        self.music_on = False

        self.pause = True

        self.allow_start = False
        self.text = ''
        self.k = 0
        self.delta = 10
        self.sign = 1

        self.recognised = False

        self.first_time = True
        self.need_input = False

        self.can_finish = False

    def message(self, screen, text='guaaaaay', x=0, y=0, color=(0, 0, 0)):
        """
        All the text you can read in the window is created here.
        :param screen: Screen where it needs to draw. Created in the method Window.create().
        :param mode: training or evaluating.
        :return:
        """
        fuente = pygame.font.Font(None, 30)

        mensaje = fuente.render(text, 1, color)

        if len(text) > 30:
            screen.blit(self.text_background_large, (x, y - 10))
        else:
            screen.blit(self.text_background, (x, y - 10))

        screen.blit(mensaje, (15 + x, 5 + y))

    def controls(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.mixer.music.stop()
                self.env.done = True
                self.input = True
                self.sleeping = True
                self.reward = 0

            elif event.type == pygame.KEYDOWN:
                if self.pause:
                    if self.allow_start:
                        if event.key == pygame.K_h:
                                self.state = Window.STATE_HAPPY
                                self.pause = False
                        elif event.key == pygame.K_n:
                                self.state = Window.STATE_NORMAL
                                self.pause = False
                        elif event.key == pygame.K_s:
                                self.state = Window.STATE_SAD
                                self.pause = False
                    else:
                        if event.key == pygame.K_BACKSPACE:
                            self.text = self.text[:-1]
                        elif event.key == pygame.K_RETURN:
                            self.input = self.text
                            self.text = ''
                        elif event.key == pygame.K_ESCAPE:
                            sys.exit()
                        else:
                            self.text += chr(event.key)
                else:
                    if event.key == pygame.K_RETURN:
                        self.input = True
                    elif event.key == pygame.K_ESCAPE:
                        self.sleeping = True
                        self.env.done = True
                        self.input = True
                        self.reward = 0
                    else:
                        if event.key == pygame.K_h:
                            self.state = Window.STATE_HAPPY
                        elif event.key == pygame.K_n:
                            self.state = Window.STATE_NORMAL
                        elif event.key == pygame.K_s:
                            self.state = Window.STATE_SAD

    def messages(self, screen):
        self.message(screen, text=self.output, x=50, y= 50)

        if self.pause:
            if self.allow_start:
                self.k += 1
                if self.k < 3:
                    self.message(screen, text='[Choose your feeling clicking H, N or S]', x=50, y=320, color= (50, 50, 0))
                elif self.k == 6:
                    self.k = 0
                self.message(screen, text='H = HAPPY', x=100, y=400, color=(50, 50, 0))
                self.message(screen, text='N = NORMAL', x=100, y=450, color=(50, 50, 0))
                self.message(screen, text='S = SAD', x=100, y=500, color=(50, 50, 0))
            if not self.recognised:
                if self.need_input:
                    self.message(screen, text='Your input : ' + self.text, x=50, y=400)
        else:
            self.k += 1
            if self.k < 3:
                self.message(screen, text='[Press ENTER to change song]', x=50, y=320, color= (128, 50, 0))
            elif self.k > 5 and self.k < 8:
                self.message(screen, text='[You can change your feeling with H, N or S]', x=50, y=320, color=(128, 50, 0))
            elif self.k == 9:
                self.k = 0
            self.message(screen, text='Your feeling : ' + self.translate(self.state),x=50, y=400)

    def drawings(self, screen):
        x, y = 500, 100 + self.delta

        if -1 < self.delta < 101:
            self.delta += self.sign*10
        else:
            if self.delta < 0:
                self.delta = 0
            elif self.delta > 100:
                self.delta = 100
            self.sign *= -1

        if self.sleeping:
            face = self.sleep_face
        elif not self.recognised:
            face = self.initial_face
        else:
            if self.pause:
                if self.first_time:
                    face = self.nervous_face
                else:
                    face = self.recognise_face
            else:
                if self.state == Window.STATE_HAPPY:
                    face = self.happy_face
                elif self.state == Window.STATE_NORMAL:
                    face = self.normal_face
                else:
                    face = self.sad_face

        screen.blit(face, (x, y))

    def translate(self, i):
        if i == Window.STATE_HAPPY:
            return "HAPPY"
        elif i == Window.STATE_NORMAL:
            return "NORMAL"
        elif i == Window.STATE_SAD:
            return "SAD"

    def create(self):
        pygame.init()
        pygame.mouse.set_visible(False)
        self.mixer.init()
        # , pygame.FULLSCREEN.
        x = 100
        y = 45
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x, y)

        screen = pygame.display.set_mode((self.width, self.height))#

        pygame.display.set_caption("Musical Assistant Recommender 2018-2019")

        x = 0
        while not self.can_finish:
            if self.sleeping:
                x += 1


            self.controls()
            time.sleep(0.4)
            screen.blit(self.background, (0, 0))
            self.messages(screen)
            self.drawings(screen)
            pygame.display.update()


    def setInput(self, input):
        self.input = input