"""
MAI 2018-2019

Cognitive Interaction with Robots

From https://www.pyimagesearch.com/2018/06/18/face-recognition-with-opencv-python-and-deep-learning by Adrian Rosebrok

Used for the face recognition part
"""
from imutils import paths
import face_recognition
import pickle
import cv2
import os


def obtain_encodings():
    """
    Function that obtains the encodings from pictures
    """
    # construct the argument parser and parse the arguments

    dataset = "pictures"

    # grab the paths to the input images in our dataset
    print("[INFO] quantifying faces...")
    imagePaths = list(paths.list_images(dataset))

    # initialize the list of known encodings and known names
    knownEncodings = []
    knownNames = []

    # loop over the image paths
    for (i, imagePath) in enumerate(imagePaths):
        # extract the person name from the image path
        print("[INFO] processing image {}/{}".format(i + 1,
        len(imagePaths)))
        name = imagePath.split(os.path.sep)[-2]

        # load the input image and convert it from RGB (OpenCV ordering)
        # to dlib ordering (RGB)
        image_org = cv2.imread(imagePath)
        image = cv2.resize(image_org, (0,0), fx = 0.4, fy = 0.4)
        rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # detect the (x, y)-coordinates of the bounding boxes
        # corresponding to each face in the input image
        boxes = face_recognition.face_locations(rgb,
        model="hog")

        # compute the facial embedding for the face
        encodings = face_recognition.face_encodings(rgb, boxes)

        # loop over the encodings
        for encoding in encodings:
        # add each encoding + name to our set of known names and
        # encodings
            knownEncodings.append(encoding)
            knownNames.append(name)

    # dump the facial encodings + names to disk
    print("[INFO] serializing encodings...")
    data = {"encodings": knownEncodings, "names": knownNames}
    f = open("pictures/encodings.pickle", "wb")
    f.write(pickle.dumps(data))
    f.close()
