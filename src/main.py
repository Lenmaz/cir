"""
MAI 2018-2019

Cognitive Interaction with Robots

@authors Antonio Romero, Manel Rodriguez, Bartomeu Pou
"""

import argparse
from qlearner import QLearner



if __name__ == '__main__':
        
    ap = argparse.ArgumentParser()
    ap.add_argument("-eps", "--epsilon", default=0.2, help="mode of recommender", type=float)
    ap.add_argument("-ve", "--void_embedding", default=False

                    )
    args = vars(ap.parse_args())

    QLearner(args['epsilon'], args['void_embedding'])