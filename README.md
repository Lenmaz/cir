# MAR: Musical assistant recommender

This project is a musical assistant recommender that learns your preferences using Heuristic Accelerated Reinforcement Learning applied to contextual bandits problem.

## Cloning the repository

Clone the repository using cmd/git bash: git clone https://gitlab.com/Lenmaz/cir.git

## Installation

To install this project you need anaconda, if you do not have please download it from: https://www.anaconda.com/download/

Additionally you need Visual Studio 2015/2017 in order to install the package dlib. There are known issues for installing dlib, if you have a problem installing it contact us.

### Automatic installation:

Access anaconda prompt and type:

1) conda create --name cir --file $pathtorequirements$\requirements_conda.txt
2) activate cir
3) pip install -r $pathtorequirements$\requirements_pip.txt 


### Manual installation:

1) conda create --name cir python=3.6.3
2) activate cir
2) conda install pandas
3) conda install numpy
4) conda install opencv=3.3.1
5) pip install cmake
6) pip install dlib
7) pip install imutils
8) pip install face_recognition
9) pip install pywin32
10) pip install pygame
11) pip install pyttsx3
12) pip install pywin32

## Running

Acces Anaconda Prompt and type:

1) activate cir
2) cd $PATHtoproject$
3) python main.py

## Bibliography

1) For the code

https://www.pyimagesearch.com/2018/06/18/face-recognition-with-opencv-python-and-deep-learning/

https://realpython.com/python-speech-recognition/

2) For Reinforcement Learning:

http://www.iiia.csic.es/~mantaras/iccbr09.pdf